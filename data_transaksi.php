<?php 

	require_once("depot.php"); //Memanggil Koneksi Database

	$sql_get = "SELECT * FROM transaksi"; //query, untuk mengambil data pada table
	$query_brg = mysqli_query($koneksi, $sql_get);

	$results = []; //menyimpan data dalam bentuk array

	//Menampilkan isi data
	while ($row = mysqli_fetch_assoc($query_brg)) {
		$results[]= $row;
	}
?>

 <!DOCTYPE html>
 <html>
 <head>
 	<title>Data Pembeli</title>
 	<link rel="stylesheet" type="text/css" href="design.css">
</head>
 <body>
 	<div id="menu">
  		<ul>
	      <a href="awaladmin.php">Beranda</a>
	          ||  
	      <a href="index.php">Logout</a>
  		</ul>
	</div>

 	<div id="judul">
		<h1 id="isi" style="font-size: 55px"><u>DATA PEMBELI</u></h1>
	</div>
 	
 	<table id="stok" cellpadding="7" cellspacing="2" width="80%">
 		<tr id="atas">
 			<td>No</td>
 			<td>Kode</td>
 			<td>Jenis</td>
 			<td>Jumlah</td>
 			<td>Total_bayar</td>
 			<td>Kategori</td>
 			<td>Keperluan</td>
 		</tr>

 		<?php 
 			$no = 1; 
 			foreach ($results as $re):
 		?>
	 		<tr>
	 			<td> <?= $re['no']; ?> </td>
	 			<td> <?= $re['kode'] ?> </td>
	 			<td> <?= $re['jenis']; ?> </td>
	 			<td> <?= $re['jumlah']; ?> </td>
	 			<td> <?= $re['total_bayar'] ?> </td>
	 			<td> <?= $re['kategori']; ?> </td>
	 			<td> <?= $re['keperluan']; ?> </td>
	 		</tr>
	 	<?php 
	 		$no++;
	 		endforeach;
	 	?>

	 	<tr>
	 		<td rowspan="3" colspan="8" style="text-align: center;"><br><br>
			<button id="sub" style="width: 250px"><a href="data.php" style="color: black;">Sebelumnya</a>
			</td>
	 	</tr>
 	</table>
 	
 	<img src="1.png" align=”bottom” style="float:right;width:600px;margin-right: -8px; margin-top: -145px;">
	
	<div class="footer">
		PRPL - [1900018025] _ &copy; ElviraPC
	</div>	

 </body>
 </html>