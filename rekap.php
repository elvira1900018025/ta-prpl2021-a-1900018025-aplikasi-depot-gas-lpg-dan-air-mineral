<?php 

	require_once("depot.php"); //Memanggil Koneksi Database

	//query, untuk mengambil data pada table
	$sql_get = "SELECT transaksi.kode, barang.jenis, SUM(jumlah) as Total_Terjual FROM transaksi INNER JOIN barang on transaksi.kode = barang.kode GROUP BY kode;"; 
	$query_brg = mysqli_query($koneksi, $sql_get);

	$results = []; //menyimpan data dalam bentuk array

	//Menampilkan isi data
	while ($row = mysqli_fetch_assoc($query_brg)) {
		$results[]= $row;
	}
?>

 <!DOCTYPE html>
 <html>
 <head>
 	<title>Rekap Jual-Beli</title>
 	<link rel="stylesheet" type="text/css" href="design.css">
</head>
<body>
 	<div id="menu">
  		<ul>
	      <a href="awaladmin.php">Beranda</a>
	          ||  
	      <a href="index.php">Logout</a>
  		</ul>
	</div>

 	<div id="judul">
 		<h1 id="isi" style="font-size: 55px"><u>REKAP JUAL - BELI</u></h1>
	</div>
 	
 	<table id="stok" cellpadding="7" cellspacing="2" width="80%">
 		<tr id="atas">
 			<td>No</td>
 			<td>Kode</td>
 			<td>Jenis</td>
 			<td>Total_Terjual</td>
 		</tr>

 		<?php 
 			$no = 1; 
 			foreach ($results as $re):
 		?>
	 		<tr>
	 			<td> <?= $no; ?> </td>
	 			<td> <?= $re["kode"] ?> </td>
	 			<td> <?= $re['jenis']; ?> </td>
	 			<td> <?= $re['Total_Terjual'] ?> </td>
	 		</tr>
	 	<?php 
	 		$no++;
	 		endforeach;
	 	?>
 	</table>
    
    <img src="1.png" align=”bottom” style="float:right;width:600px;margin-right: -8px; margin-top: -41px;">
	
	<div class="footer">
		PRPL - [1900018025] _ &copy; ElviraPC
	</div>	

 </body>
 </html>