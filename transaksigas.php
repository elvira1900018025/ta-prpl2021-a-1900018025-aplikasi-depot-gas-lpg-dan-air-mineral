<?php 

require_once("depot.php");

?>

<!DOCTYPE html>
<html>
<head>
	<title>Transaksi Beli GAS</title>
	<link rel="stylesheet" type="text/css" href="design.css">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
</head>
<body>
	<div id="menu">
  		<ul>
	      <a href="pembeli.php">Beranda</a>
	          ||  
	      <a href="index.php">Keluar</a>
  		</ul>
	</div>

	<h1 id="kanan">PESAN <br>GAS LPG</h1>
	
	<img src="keranjang1.png" align=”bottom” style="float:right;width:245px;margin-right: -40px; margin-top: 75px;">

<div class="box_tran">
	<form action="transaksigas.php" method="POST">
		<table id="t2"  cellpadding="7" cellspacing="2">
			<tr>
				<td>Kategori</td>
				<td> : </td>
				<td>
					<select name="kategori">
						<option disabled selected> RT/UM/Lainnya </option>
						<option>RT</option>
						<option>UM</option>
						<option>Lainnya</option>
					</select>
				</td>
			</tr>

			<tr>
				<td>Keperluan</td>
				<td> : </td>
				<td>
					<input type="text" name="keperluan">
				</td>
			</tr>

			<tr>
				<td>Jenis Gas LPG</td>
				<td> : </td>
				<td>
					<select name="kode">
						<option disabled selected> </option>
				 		<?php
				 		$query = "SELECT * FROM barang ORDER BY kode DESC LIMIT 3";
				 		$sql = mysqli_query($koneksi, $query); 
				 			while ($kode=mysqli_fetch_array($sql)) 
				 			{
				 				?>
								<option value="<?=$kode['kode']?>" ><?=$kode['kode']?> -> <?=$kode['jenis'];?> : Rp <?=$kode['harga']?></option>
								<?php  	
						 	}
					 	?>
				</td>
			</tr>

			<tr>
				<td>Jumlah</td>
				<td> : </td>
				<td>
					<input type="text" name="jumlah">
				</td>
			</tr>		

		</table>
		
		<button id="sub" type="submit" name="submit" style="width: 100px;">Hitung</button>
		<button id="sub" style="width: 100px;"><a href="pesan.php">PESAN</a></button> 
	
	</form>
	<hr>
	<?php
		if(isset($_POST['submit'])){
			$kode = $_POST['kode'];
			$kategori = $_POST['kategori'];
			$keperluan = $_POST['keperluan'];
			
			$jumlah = $_POST['jumlah'];
			
			$queryHitung = "SELECT * FROM barang WHERE kode='$kode'";
            $sqlHitung = mysqli_query($koneksi, $queryHitung);
            $resultHitung = mysqli_fetch_assoc($sqlHitung);
            $jenis = $resultHitung['jenis'];
            $harga = $resultHitung['harga'];
            $total_bayar = $resultHitung['harga']*$jumlah;
 			
			echo "
			<h2>-> Cek Pesanan Anda <-</h2>
				<table id='stok' border='1' style='text-align: center;''>	
					<tr id='atas'>
						<th>Jenis</th>
						<th>Harga</th>
						<th>Jumlah</th>
						<th>Total Bayar</th>
					</tr>
					<tr>
						<td>$jenis</td>
						<td>$harga</td>
						<td>$jumlah</td>
						<td>$total_bayar</td>
					</tr>
				</table>
			";
		try {
            $queryPembeli  =   "INSERT INTO transaksi VALUES ('','$kode','$kategori','$keperluan','$jenis','$jumlah','$total_bayar')";
            $resultPembeli = mysqli_query($koneksi, $queryPembeli);
        }catch(Exception $e) {
            var_dump($e);
        }
	}
?>
</div>

<img src="1.png" align=”bottom” style="float:right;width:600px;margin-right: -8px; margin-top: -355px;">
	
	<div class="footer">
		PRPL - [1900018025] _ &copy; ElviraPC
	</div>	
</body>
</html>