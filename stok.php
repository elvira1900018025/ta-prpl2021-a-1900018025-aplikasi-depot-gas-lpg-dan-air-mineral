<?php 

	require_once("depot.php"); //Memanggil Koneksi Database

	$sql_get = "SELECT * FROM barang"; //query, untuk mengambil data pada table
	$query_brg = mysqli_query($koneksi, $sql_get);

	$results = []; //menyimpan data dalam bentuk array

	//Menampilkan isi data
	while ($row = mysqli_fetch_assoc($query_brg)) {
		$results[]= $row;
	}
?>

 <!DOCTYPE html>
 <html>
 <head>
 	<title>Stok</title>
 	<link rel="stylesheet" type="text/css" href="design.css">
</head>
<body>
	<div id="menu">
  		<ul>
	      <a href="awaladmin.php">Beranda</a>
	          ||  
	      <a href="index.php">Logout</a>
  		</ul>
	</div>

 	<h1 id="isi" style="font-size: 60px"><u>STOK</u></h1>
	

 	<table id="stok" cellpadding="7" cellspacing="2" width="80%" style="font-family: Repo;">
 		<tr id="atas">
 			<td>No</td>
 			<td>Kode</td>
 			<td>Jenis</td>
 			<td>Harga [Rp]</td>
 			<td>Stok</td>
 			<td>CRUD</td>
 		</tr>

 		<?php 
 			$no = 1; 
 			foreach ($results as $re):
 		?>
	 		<tr>
	 			<td> <?= $no; ?> </td>
	 			<td> <?= $re['kode'] ?> </td>
	 			<td> <?= $re['jenis']; ?> </td>
	 			<td> <?= $re['harga'] ?> </td>
	 			<td> <?= $re['stok'] ?> </td>
	 			<td>
	 				<a href="update.php?kode=<?=$re['kode'];?>">Update</a>      ||   
	 				<a href="del.php?kode=<?=$re['kode'];?>">Delete</a>
	 			</td>
	 		</tr>
	 	<?php 
	 		$no++;
	 		endforeach;
	 	?>

	 	<tr>
	 		<td rowspan="3" colspan="8" style="text-align: center;">
	 		<button  id="sub" style="width: 250px; color: black;"><a href="item.php" style="color: black;">Tambah Item</a></button>
			<br><br>
			</td>
	 	</tr>
 	</table>

 	<img src="1.png" align=”bottom” style="float:right;width:600px;margin-right: -8.3px; margin-top: -296px;">

	<div class="footer">
		PRPL - [1900018025] _ &copy; ElviraPC
	</div>	

 </body>
 </html>