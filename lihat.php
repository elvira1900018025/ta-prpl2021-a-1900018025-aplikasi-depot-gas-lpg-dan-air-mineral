<?php 

	require_once("depot.php"); //Memanggil Koneksi Database

	$sql_get = "SELECT * FROM barang"; //query, untuk mengambil data pada table
	$query_brg = mysqli_query($koneksi, $sql_get);

	$results = []; //menyimpan data dalam bentuk array

	//Menampilkan isi data
	while ($row = mysqli_fetch_assoc($query_brg)) {
		$results[]= $row;
	}
?>

 <!DOCTYPE html>
 <html>
 <head>
 	<title>Stok</title>
 	<link rel="stylesheet" type="text/css" href="design.css">
</head>
<body>
	<div id="menu">
  		<ul>
	      <a href="pembeli.php">Beranda</a>
	          ||  
	      <a href="index.php">Keluar</a>
  		</ul>
	</div>

 	<div id="isi">
 		<h1><u>STOK</u></h1>
	</div>


 	<table id="stok" cellpadding="7" cellspacing="2" width="75%">
 		<tr id="atas">
 			<td>No</td>
 			<td>Kode</td>
 			<td>Jenis</td>
 			<td>Harga [Rp]</td>
 			<td>Stok</td>
 		</tr>

 		<?php 
 			$no = 1; 
 			foreach ($results as $re):
 		?>
	 		<tr>
	 			<td> <?= $no; ?> </td>
	 			<td> <?= $re['kode'] ?> </td>
	 			<td> <?= $re['jenis']; ?> </td>
	 			<td> <?= $re['harga'] ?> </td>
	 			<td> <?= $re['stok'] ?> </td>
	 		</tr>
	 	<?php 
	 		$no++;
	 		endforeach;
	 	?>
 	</table>

 	<img src="1.png" align=”bottom” style="float:right;width:600px;margin-right: -8.3px; margin-top: -156px;">

	<div class="footer">
		PRPL - [1900018025] _ &copy; ElviraPC
	</div>
 </body>
 </html>